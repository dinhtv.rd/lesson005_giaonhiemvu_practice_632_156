class TTSSlotUtils(object):
    @staticmethod
    def process_customer_phone(phone):
        phone = phone[:4] + "," + phone[4:]
        phone = " ".join(phone)
        return phone
