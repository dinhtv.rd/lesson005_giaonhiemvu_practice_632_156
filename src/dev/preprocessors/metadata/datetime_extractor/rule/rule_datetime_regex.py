import json
import re

from src.dev.preprocessors.metadata.datetime_extractor.define import (
    PATH_RULE_DATETIME_REGEX,
)


def better(x, y):
    """
    Entity selection function
    Default: The choice to appear position and length priority
    Custom: The choice depends on the type of entity
    """

    # Default: position ahead & longer
    if x["end"] - 1 < y["start"]:
        return x
    if y["end"] - 1 < x["start"]:
        return y
    if len(x["value"]) > len(y["value"]):
        return x
    if len(y["value"]) > len(x["value"]):
        return y
    return y


class RuleDatetimeRegex(object):
    def __init__(self) -> None:
        with open(PATH_RULE_DATETIME_REGEX, encoding="utf8") as file_regex:
            self.regex = json.load(file_regex)
        self.DATE = "date"
        self.TIME = "time"
        self.SESSION = "session"
        self.NUMBER = "number"

    def process(self, message):
        result = {
            self.DATE: None,
            self.TIME: None,
            self.SESSION: None,
            self.NUMBER: None
        }
        list_regex_entity = self.regex.get("entity")

        for regex_entity in list_regex_entity:
            entity_name = regex_entity.get("name")
            if entity_name in result:
                entities = []
                regex = regex_entity.get("regex")
                for pattern_dict in regex:
                    type_entity = pattern_dict.get("type")
                    patterns = pattern_dict.get("pattern")
                    for pattern in patterns:
                        pattern = re.compile(pattern)
                        for match in pattern.finditer(message.lower()):
                            entity = {
                                "start": match.start(),
                                "end": match.end(),
                                "value": match.group(),
                                "type": type_entity
                            }
                            entities.append(entity)
                if entities:
                    choose = entities[0]
                    for e in entities[1:]:
                        choose = better(e, choose)
                    result[entity_name] = choose.get("value")
        return result
