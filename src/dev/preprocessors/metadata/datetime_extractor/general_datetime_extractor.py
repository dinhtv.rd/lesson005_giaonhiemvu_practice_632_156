from src.dev.preprocessors.metadata.datetime_extractor.duckling.duckling_datetime_extractor import \
    DucklingDatetimeExtractor
from src.dev.preprocessors.metadata.datetime_extractor.rule.rule_datetime_extractor import RuleDatetimeExtractor
from src.dev.preprocessors.metadata.datetime_extractor.define import PIPELINE
import logging

logger = logging.getLogger(__name__)


class GeneralDatetimeExtractor(object):
    extractors = {
        "Rule": RuleDatetimeExtractor,
        "Duckling": DucklingDatetimeExtractor
    }

    def __init__(self) -> None:
        self.pipeline = []
        for extractor_name in PIPELINE:
            if extractor_name not in self.extractors:
                logger.warning(f"{extractor_name} wasn't defined!")
            else:
                self.pipeline.append(self.extractors.get(extractor_name)())

    async def process(self, message,
                      date_from=None,
                      date_to=None,
                      time_from=None,
                      time_to=None,
                      priority_direction=None):
        date = None
        time = None
        for extractor in self.pipeline:
            result = await extractor.process(message,
                                             date_from=date_from,
                                             date_to=date_to,
                                             time_from=time_from,
                                             time_to=time_to,
                                             priority_direction=priority_direction)
            # print(result)
            if date is None:
                date = result.get("date")
            if time is None:
                time = result.get("time")
        return {
            "date": date,
            "time": time
        }

# from datetime import datetime
#
# message = "giờ hành chính"
# extractor = GeneralDatetimeExtractor()
# time_from = "08:00:00"
# time_to = "18:00:00"
# print(extractor.process(message, "FORWARD", time_from=time_from, time_to=time_to))
