import os
import re
import logging
import json
from typing import Tuple, Optional, Dict, List
import copy

CONFIDENCE_ZERO_ENTITY = 0
CONFIDENCE_CORRECT_ENTITY = 1.25
CONFIDENCE_NORMAL_ENTITY = 1
CONFIDENCE_WRONG_ENTITY = 0.75

logger = logging.getLogger(__name__)


class ExtractorEntityAddress:
    def __init__(self, regex_process_entity, norm_number, dir_address):
        self.regex_process_entity = regex_process_entity
        self.norm_number = norm_number
        self.load_data_address_json(dir_address=dir_address)
        self.province_tp = [
            "hà nội",
            "hồ chí minh",
            "hải phòng",
            "cần thơ",
            "đã nẵng",
        ]
        self.list_number_text = [
            "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín", "mười", "trăm", "nghìn", "triệu", "lẻ",
            "linh", "ninh", "lăm", "không", "mốt", "tư", "bẩy", "mươi",
        ]

    def load_data_address_json(self, dir_address: str):
        """
        Load tất cả CSDL địa chỉ vào các biến
        :param dir_address:
        :return:
        """

        def load_json(filename, encoding='utf-8') -> Optional:
            with open(filename, "r", encoding=encoding) as file:
                return json.load(file)

        self.database_address = load_json(os.path.join(dir_address, "database_address_ver2.json"))

        self.province2province = load_json(os.path.join(dir_address, "province2province.json"))
        self.province2prefix = load_json(os.path.join(dir_address, "province2prefix.json"))

        self.district2district = load_json(os.path.join(dir_address, "district2district.json"))
        self.district2prefix = load_json(os.path.join(dir_address, "district2prefix.json"))
        self.district2province = load_json(os.path.join(dir_address, "district2province.json"))

        self.ward2ward = load_json(os.path.join(dir_address, "ward2ward.json"))
        self.ward2prefix = load_json(os.path.join(dir_address, "ward2prefix.json"))
        self.ward2district = load_json(os.path.join(dir_address, "ward2district.json"))
        self.ward2province = load_json(os.path.join(dir_address, "ward2province.json"))

        self.district_like_province = {}  ## tên district giống với tên province
        self.ward_like_province = {}  ## tên phường xã, giống với tên tỉnh, thành phố
        self.ward_like_district = {}  ## tên phường xã giống với tên quận huyện

        province_check = {}
        for province_name in self.province2province:
            province_check[province_name] = True
        district_check = {}
        for district_name in self.district2district:
            district_check[district_name] = True

        for district in self.district2province:
            if province_check.get(district) is not None and self.district_like_province.get(district) is None:
                self.district_like_province[district] = True

        self.district2wards = {}  ## 1 district có thể gồm nhiều xã
        for ward in self.ward2district:
            ## append vao district2ward
            list_district = self.ward2district.get(ward)
            for district in list_district:
                if self.district2wards.get(district) is None:
                    self.district2wards[district] = []
                if ward not in self.district2wards[district]:
                    self.district2wards[district].append(ward)
            ## các tên ward giống với tên của district
            if district_check.get(ward) is not None and self.ward_like_district.get(ward) is None:
                self.ward_like_district[ward] = True

        self.province2wards = {}  ## 1 province có thể gồm nhiều xã
        for ward in self.ward2province:
            list_province = self.ward2province.get(ward)
            for province in list_province:
                if self.province2wards.get(province) is None:
                    self.province2wards[province] = []
                if ward not in self.province2wards[province]:
                    self.province2wards[province].append(ward)
            if province_check.get(ward) is not None and self.ward_like_province.get(ward) is None:
                self.ward_like_province[ward] = True

    def check_district_province(self, province: str, district: str) -> bool:
        """
        Hàm kiểm tra sự đồng nhất giữa district và province
        :param province:
        :param district:
        :return:
        """
        if province is None and district is None:
            return False
        if province is None:
            return True if self.district2district.get(district) is not None else False
        if district is None:
            return True if self.province2province.get(province) is not None else False
        if self.district2province.get(self.district2district.get(district)) is None:
            return False
        return True if self.province2province.get(province) in self.district2province.get(
            self.district2district.get(district)) else False

    def check_exit_ward(self, province, district, ward) -> bool:
        """
        Hàm kiểm tra sự tồn tại ward xem có đồng nhất với province và district hay không
        :param province:
        :param district:
        :param ward:
        :return:
        """
        if province is None and district is None and ward is not None:
            return True
        if province is None and district is None:
            return False
        if ward is not None:
            ward_map = self.ward2ward.get(ward)
            if district is not None:
                district_map = self.district2district.get(district)
                if province is not None:
                    province_map = self.province2province.get(province)
                    if self.database_address.get(ward_map) is None or \
                            self.database_address.get(ward_map).get(district_map) is None or \
                            self.database_address.get(ward_map).get(district_map).get(province_map) is None:
                        return False
                else:
                    if self.ward2district.get(ward_map) is None or district_map not in self.ward2district.get(ward_map):
                        return False
            else:
                if province is not None:
                    province_map = self.province2province.get(province)
                    if self.ward2province.get(ward_map) is None or province_map not in self.ward2province.get(ward_map):
                        return False
        return True

    def find_district_miss(self, province: str, ward: str) -> str:
        """
        Hàm tìm district khi đã biết province và ward
        :param province:
        :param ward:
        :return:
        """
        if ward is None:
            return None
        list_district = self.ward2district.get(ward)
        result = []
        for district in list_district:
            list_province = self.district2province.get(district)
            if province is None and len(list_province) == 1:
                result.append(district)
            if province is not None and province in list_province:
                result.append(district)
        if len(result) == 1:
            return result[0]
        return None

    def find_province_miss(self, district: str, ward: str):
        """
        Hàm tìm province khi đã biết district và ward
        :param district:
        :param ward:
        :return:
        """
        if district is not None and ward is not None:
            # list_province_1 = self.district2province.get(district)
            list_province_1 = [name for name in self.database_address.get(self.ward2ward.get(ward)).get(
                self.district2district.get(district))]
            if list_province_1 is not None and len(list_province_1) == 1:
                return list_province_1[0]
            else:
                list_province_2 = self.ward2province.get(ward)
                if list_province_2 is None or list_province_1 is None:
                    return None
                result_province = []
                for province_1 in list_province_1:
                    for province_2 in list_province_2:
                        if province_1 == province_2:
                            result_province.append(province_1)
                if len(result_province) == 1:
                    return result_province[0]

        if district is not None:
            list_province_1 = self.district2province.get(district)
            if list_province_1 is not None and len(list_province_1) == 1:
                return list_province_1[0]

        if ward is not None:
            list_province_2 = self.ward2province.get(ward)
            if list_province_2 is not None and len(list_province_2) == 1:
                return list_province_2[0]
        return None

    def check_number(self, token: str) -> bool:
        """
        Kiểm tra token có phải number hay không
        :param token:
        :return:
        """
        if token is None or len(token) == 0:
            return False
        for c in token:
            if c < '0' or c > '9':
                return False
        return True

    def check_is_number_text(self, text: str) -> bool:
        """
        Kiểm tra đoạn text có phải là số hay không
        :param text:
        :return:
        """
        if text is None or len(text) == 0:
            return False
        if self.check_number(text) == True:
            return True
        for token in text.split(" "):
            if token not in self.list_number_text:
                return False
        return True

    def norm_text(self, text) -> str:
        """
        Chuẩn hóa lại khoảng trắng trong text
        :param text:
        :return: text sau khi chuẩn hóa
        """
        if text is None:
            return None
        tokens = text.split(" ")
        result = []
        for token in tokens:
            if len(token) > 0:
                result.append(token)
        return " ".join(result)


class ExtractorLayerAddress(ExtractorEntityAddress):
    def process(self, message: str, province: str = None, district: str = None, ward: str = None,
                status_regex_all_ward: bool = False) -> Tuple:
        """
        Hàm xử lý tách tầng địa chỉ có trong một đoạn text. Chỉ đến tầng xã, huyện, tỉnh
        :param message: Đầu vào text
        :param province: tên tỉnh, thành phố
        :param district: tên quận, huyện
        :param ward: tên phường,xã
        :param status_regex_all_ward: trạng thái regex hết tất cả các xã trong trường hợp không tin district và province
        :return(dict) : đầu ra dict chứa thông tin phân tách tần địa chỉ gồm
        {
            'ward': ..., ## tên phường, xã, thị trấn
            'district': ..., ## tên của quận, huyện, thị xã, thành phố
            'province': ..., ## tên tỉnh, thành phố
            'confidence' : ..., ## Độ tin cậy của phần trích rút thông tin district và province
            'confidence_ward': ..., ## Độ tin cậy của phần tích trích rút thông tin ward.
        }
        """
        message = message.lower()
        message_norm = self.norm_number.process(message)  ## Thực hiện norm các con số trong message lại
        pairs, list_entiy = self.process_regex_district_and_province(message=message_norm, return_list_entity=True)
        # print ("pairs = ", pairs) ## pairs: List[dict]

        ## thực hiện regex ward với pairs<district,entity>
        if pairs is None or len(pairs) == 0:
            if province is not None or district is not None:
                ward_entity, ward_confidence = self.process_regex_ward(
                    message_norm,
                    district,
                    province)
                pair_end = {
                    'province': province,
                    'district': district,
                    'ward': ward_entity.get("value") if ward_entity is not None else ward,
                    'ward_confidence': ward_confidence,
                }
                return pair_end
            if status_regex_all_ward == True:
                ward_entity, ward_confidence = self.process_regex_ward(
                    message_norm,
                    district,
                    province,
                    status_regex_all_ward=status_regex_all_ward)
                print(f"{ward_entity, ward_confidence}")
                if ward_confidence is None or ward_confidence < CONFIDENCE_CORRECT_ENTITY:
                    return None
                pair_end = {
                    'province': None,
                    'district': None,
                    'ward': ward_entity.get("value") if ward_entity is not None else ward,
                    'ward_confidence': ward_confidence,
                }
                pair_end = self.map_pair_norm(pair_end)
                pair_end = self.fill_entity_miss(pair_end)
                return pair_end
            return None
        list_entiy = sorted(list_entiy, key=lambda x: x['confidence'], reverse=True)
        ## Giá trị max confidence của district,province
        max_confidence_district_province = max([element.get("confidence") for element in pairs])
        pair_end = None
        max_confidence_ward = -1
        ## Thực hiện regex ward với cặp <district,province>
        for element in pairs:
            if element.get("confidence") < max_confidence_district_province:
                continue
            entities = []
            check_append_entity = {}
            for entity in list_entiy:
                if element.get("district") is not None and entity.get("entity") == "district" and element.get(
                        "district") == entity.get("value"):
                    if check_append_entity.get("district") is None:
                        entities.append(entity)
                        check_append_entity["district"] = True
                    if entity.get("confidence") == CONFIDENCE_WRONG_ENTITY:
                        continue
                    entities.append(entity)
                if element.get("province") is not None and entity.get("entity") == "province" and element.get(
                        "province") == entity.get("value"):
                    if check_append_entity.get("province") is None:
                        entities.append(entity)
                        check_append_entity["province"] = True
                    if entity.get("confidence") == CONFIDENCE_WRONG_ENTITY:
                        continue
                    entities.append(entity)
                if entity.get("entity") in ["keyword_district", "keyword_province"]:
                    entities.append(entity)
            ## Replace các word không phải ward
            message_remove_district_province = self.regex_process_entity.remove_text_from_entities(message_norm,
                                                                                                   entities)
            # print ("message_remove_district_province: ", message_remove_district_province)
            ward_entity, ward_confidence = self.process_regex_ward(
                message_remove_district_province,
                element.get("district"),
                element.get("province"))
            if pair_end is None:
                pair_end = copy.deepcopy(element)
            if ward_entity is not None and ward_confidence is not None and max_confidence_ward < ward_confidence:
                max_confidence_ward = ward_confidence
                pair_end = copy.deepcopy(element)
                pair_end["ward"] = ward_entity.get("value")
                pair_end["ward_confidence"] = max_confidence_ward
        pair_end = self.map_pair_norm(pair_end)
        pair_end = self.fill_entity_miss(pair_end)
        return pair_end

    def process_regex_district_and_province(self, message: str, return_list_entity: bool = False) -> Dict:
        """
        Thực hiện trích rút district và province
        :param message:
        :param return_list_entity: Tráng thái có muốn trả về thêm List[entity] không
        :return (Dict/None): {
            ''province' : ,
            'district' : ,
            'confidence' : ,
        }
        """
        list_entity_all = self.regex_process_entity.get_extract_entities(message,
                                                                         self.regex_process_entity.regex_feature.get(
                                                                             "ObjectAddress"), status_space=True)
        list_entity = self.regex_process_entity.remove_entites_overlap(list_entity_all)  ## Remove overlap entity
        extractor = {
            "province": [],
            "district": [],
        }
        keyword_address = ["keyword_street", "keyword_ward", "keyword_district", "keyword_province"]
        list_entity = sorted(list_entity, key=lambda x: x['start'], reverse=True)
        for i, entity in enumerate(list_entity):
            if entity.get("entity") in ["district", "province"]:
                if i + 1 == len(list_entity):
                    if entity.get("entity") == "district" and entity.get("value").find("quận ") != -1:
                        entity["confidence"] = CONFIDENCE_CORRECT_ENTITY
                if i + 1 < len(list_entity):
                    if entity.get("entity") == "district" and list_entity[i + 1].get("entity") in keyword_address and \
                            list_entity[i + 1].get("end") + 1 == entity.get("start"):
                        if list_entity[i + 1].get("entity") == "keyword_district":
                            entity["confidence"] = CONFIDENCE_CORRECT_ENTITY
                        else:
                            if list_entity[i + 1].get("value") == "thành phố":
                                entity["confidence"] = CONFIDENCE_CORRECT_ENTITY
                            else:
                                entity["confidence"] = CONFIDENCE_WRONG_ENTITY
                    else:
                        ## Trường quận xx thì gán confidence = CONFIDENCE_CORRECT_ENTITY
                        if entity.get("entity") == "district" and entity.get("value").find("quận ") != -1:
                            entity["confidence"] = CONFIDENCE_CORRECT_ENTITY
                    if entity.get("entity") == "province" and list_entity[i + 1].get("entity") in keyword_address and \
                            list_entity[i + 1].get("end") + 1 == entity.get("start"):
                        if list_entity[i + 1].get("entity") == "keyword_province":
                            if (list_entity[i + 1].get("value") == "thành phố" and entity.get(
                                    "value") in self.province_tp) or list_entity[i + 1].get("value") == "tỉnh":
                                entity["confidence"] = CONFIDENCE_CORRECT_ENTITY
                            else:
                                if self.district_like_province.get(entity.get("value")) == True:
                                    entity_new = copy.deepcopy(entity)
                                    entity_new["entity"] = "district"
                                    entity_new["confidence"] = CONFIDENCE_CORRECT_ENTITY
                                    extractor["district"].append(copy.deepcopy(entity_new))
                                    continue
                        else:
                            if list_entity[i + 1].get(
                                    "entity") == "keyword_district" and self.district_like_province.get(
                                entity.get("value")) == True:
                                entity_new = copy.deepcopy(entity)
                                entity_new["entity"] = "district"
                                entity_new["confidence"] = CONFIDENCE_CORRECT_ENTITY
                                extractor["district"].append(copy.deepcopy(entity_new))
                                continue
                            entity["confidence"] = CONFIDENCE_WRONG_ENTITY
                extractor[entity.get("entity")].append(copy.deepcopy(entity))
        pairs = self.process_pairs_district_province(extractor)
        if len(pairs) == 0:
            if return_list_entity == True:
                return None, list_entity
            return None
        # print("pairs (province, district): ", pairs)
        pairs = sorted(pairs, key=lambda x: x['confidence'], reverse=True)
        pairs_tmp = []
        for pair in pairs:
            if pair not in pairs_tmp:
                pairs_tmp.append(pair)
        if return_list_entity == True:
            return pairs_tmp, list_entity
        return pairs_tmp

    def process_regex_ward(self, message: str, district: str, province: str, status_regex_all_ward=False) -> Tuple:
        """
        Hàm xử lý regex ward
        :param message:
        :param district: tên quận,huyện
        :param province: tên tỉnh,thành phố
        :param status_regex_all_ward: Trạng thái regex tất cả các ward trong TH district và province is None
        :return Tuple: entity ward trích rút và độ tin cậy của entity
        ward_entity_best: Entity ward tốt nhất
        max_confidence_ward: độ tin cậy của entity ward
        """
        if district is not None and self.district2district.get(district) is not None:
            list_ward = self.district2wards.get(self.district2district.get(district))
            if list_ward is None or len(list_ward) == 0:
                return None, None
            return self.regex_ward(message, district, province, list_ward)

        if province is not None and self.province2province.get(province) is not None:
            list_ward = self.province2wards.get(self.province2province.get(province))
            if list_ward is None or len(list_ward) == 0:
                return None, None
            return self.regex_ward(message, district, province, list_ward)

        if province is None and district is None and status_regex_all_ward == True:
            list_ward = [name for name in self.ward2ward]
            if list_ward is None or len(list_ward) == 0:
                return None, None
            return self.regex_ward(message, district, province, list_ward)
        return None, None

    def regex_ward(self, message: str, district: str, province: str, list_ward: List) -> Tuple:
        """
        Thực hiện regex ward có trong danh sách list_ward
        :param message:
        :param district:
        :param province:
        :param list_ward:
        :return (Tuple): ward_entity_best, max_confidence_ward
        ward_entity_best: Entity ward tốt nhất
        max_confidence_ward: độ tin cậy của entity ward
        """
        if list_ward is None or len(list_ward) == 0:
            return None, None
        pattern_regex_ward = "|".join(sorted(list_ward, key=lambda x: len(x), reverse=True))
        pattern_regex_ward = f"({pattern_regex_ward})"
        regex_feature_ward = {
            'ward': {
                "pattern": [pattern_regex_ward]
            },
            'keyword_ward': {
                "pattern": ["xã", "phường", "thị trấn"]
            }
        }
        list_extract_all_ward = self.regex_process_entity.get_extract_entities(message=message,
                                                                               regex_feature=regex_feature_ward,
                                                                               status_space=True)
        list_extract_ward = self.regex_process_entity.remove_entites_overlap(list_extract_all_ward)
        if list_extract_ward is None or len(list_extract_ward) == 0:
            return None, None
        list_extract_ward = sorted(list_extract_ward, key=lambda x: x['start'], reverse=True)
        max_confidence_ward = -1
        ward_entity_best = None
        for i, entity in enumerate(list_extract_ward):
            if entity.get("entity") != "ward":
                continue
            if i + 1 < len(list_extract_ward) and list_extract_ward[i + 1].get("entity") == "keyword_ward" and \
                    list_extract_ward[i + 1].get("end") + 1 == entity.get("start"):
                entity["confidence"] = CONFIDENCE_CORRECT_ENTITY
            if entity.get("value").find("phường ") != -1:
                entity["confidence"] = CONFIDENCE_CORRECT_ENTITY
            ward = entity.get("value")
            confidence = 0
            # if (district is not None and ward == district) or (province is not None and ward == province):
            # 	if message.count(ward) <= 1:
            # 		continue
            if district is not None and self.district2district.get(district) is not None:
                list_district = self.ward2district.get(ward)
                if self.district2district.get(district) in list_district:
                    confidence = 3 if self.ward_like_district.get(ward) == True or self.ward_like_province.get(
                        ward) == True else 4
                else:
                    confidence = 0
            else:
                if province is not None and self.province2province.get(province) is not None:
                    list_province = self.ward2province.get(ward)
                    if self.province2province.get(province) in list_province:
                        confidence = 3 if self.ward_like_province.get(ward) == True or self.ward_like_district.get(
                            ward) == True else 4
                else:
                    confidence = 1
            if self.check_exit_ward(province, district, ward) == False:
                continue
            confidence = confidence * entity.get("confidence")
            ward_entity_best = entity if confidence > max_confidence_ward else ward_entity_best
            max_confidence_ward = max(confidence, max_confidence_ward)
        return ward_entity_best, max_confidence_ward

    def process_pairs_district_province(self, extractor: Dict) -> List[dict]:
        """
        Xử lý trích rút ra các cặp province và district với confidence.
        ## Giá trị confidence của các pair sẽ là : 0 -> 4:
        ## confidence = 0: district và province không đồng nhất trên CSDL. khi map district to province thì giá trị khác province thực
        ## confidence = 0.25: province có giá trị còn district có giá trị không giống với ward, và district và province không đồng nhất trên CSDL
        ## confidence = 0.5: province có giá trị còn district có giá trị giống với ward, và district và province không đồng nhất trên CSDL. nhưng ward và province lại đồng nhất CSDL
        ## confidence = 1: province có giá trị còn district = None
        ## confidence = 2: province và district có cùng giá trị
        ## confidence = 3: province và district không cùng giá trị và Thỏa mãn điều kiện ánh Map district2province. Province không xuất hiện trong message gửi mà được MAP district2province.
        ## confidence = 4: province và district không cùng giá trị và Thỏa mãn điều kiện ánh Map district2province. Province có xuất hiện trong message gửi.
        :param extractor Dict: {
            "province" : [], ## Chứa một list entity province được trích rút
            "district" : [], ## Chứa một list entity district được trích rút
        }
        :return (List): pairs các cặp district và province
        [
            {
            'province' : ...,
            'district' : ...,
            'confidence' : ...,
            },
            ....
        ]
        """
        if extractor is None:
            return []
        # print (f"extractor province district = {extractor}")
        pairs = []
        if len(extractor.get("province")) > 0:
            for entity_province in extractor.get("province"):
                province = entity_province.get("value")
                district = None

                ## khi district bằng None
                if len(extractor.get("district")) == 0:
                    confidence = 1 * entity_province.get("confidence")
                    pairs.append(copy.deepcopy(self.setup_dict_pair(province, None, confidence)))
                    continue

                for entity_district in extractor.get("district"):
                    district = entity_district.get("value")
                    if self.ward_like_district.get(district) == True:  ## District cùng tên với ward
                        if self.ward2ward.get(district) is None or self.province2province.get(province) is None:
                            print(f"ERROR Mapping IS None: district (ward) {district} and province = {province}")
                            continue
                        if self.province2province.get(province) in self.ward2province.get(self.ward2ward.get(district)):
                            if self.province2province.get(province) in self.district2province.get(
                                    self.district2district.get(district)):
                                confidence = 3 * entity_district.get("confidence")
                                pairs.append(copy.deepcopy(self.setup_dict_pair(province, district, confidence)))
                                if entity_district.get("confidence") == CONFIDENCE_WRONG_ENTITY:
                                    pairs.append(copy.deepcopy(self.setup_dict_pair(province, None, confidence)))
                            else:
                                confidence = 0.5
                                pairs.append(copy.deepcopy(self.setup_dict_pair(province, None,
                                                                                confidence * entity_province.get(
                                                                                    "confidence"))))
                                pairs.append(copy.deepcopy(self.setup_dict_pair(None, district,
                                                                                confidence * entity_district.get(
                                                                                    "confidence"))))
                        else:
                            confidence = 0
                            pairs.append(copy.deepcopy(self.setup_dict_pair(province, None, confidence)))

                    if self.district2district.get(district) is not None and \
                            self.district2province.get(self.district2district.get(district)) is not None:
                        list_province = self.district2province.get(self.district2district.get(district))
                        if province in list_province:
                            if self.ward_like_district.get(district) == True:
                                pairs.append(copy.deepcopy(
                                    self.setup_dict_pair(province, district, 3 * entity_district.get("confidence"))))
                            else:
                                pairs.append(copy.deepcopy(
                                    self.setup_dict_pair(province, district, 4 * entity_district.get("confidence"))))
                        else:
                            pairs.append(copy.deepcopy(
                                self.setup_dict_pair(province, None, 0.25 * entity_province.get("confidence"))))
                            pairs.append(copy.deepcopy(
                                self.setup_dict_pair(None, district, 0.25 * entity_district.get("confidence"))))
                        for province_map in list_province:
                            if self.province2province.get(province_map) == self.province2province.get(province):
                                if self.ward_like_district.get(district) == True and entity_district.get(
                                        "confidence") == CONFIDENCE_WRONG_ENTITY:
                                    confidence = 3 * entity_district.get("confidence")
                                else:
                                    confidence = 4 * entity_district.get("confidence")
                                pairs.append(copy.deepcopy(
                                    self.setup_dict_pair(self.province2province.get(province_map), district,
                                                         confidence)))
                            else:
                                confidence = 0
                                pairs.append(copy.deepcopy(
                                    self.setup_dict_pair(self.province2province.get(province_map), district,
                                                         confidence)))
                    else:
                        print(
                            f"ERRPR MAPPING district = {district}. district2district = {self.district2district.get(district)}. and district2province = {self.district2province.get(self.district2district.get(district))}")
            return pairs

        for entity_district in extractor.get("district"):
            district = entity_district.get("value")
            if self.district2district.get(district) is not None and \
                    self.district2province.get(self.district2district.get(district)) is not None:
                pairs.append(copy.deepcopy(self.setup_dict_pair(None, district, 1 * entity_district.get("confidence"))))
            else:
                print(
                    f"ERRPR MAPPING district = {district}. district2district = {self.district2district.get(district)}. and district2province = {self.district2province.get(self.district2district.get(district))}")
        return pairs

    def setup_dict_pair(self, province: str, district: str, confidence: float) -> Dict:
        """
        Tạo một dict pair với đầu vào là 3 trường thông tin
        :param province:
        :param district:
        :param confidence:
        :return (Dict): {
            'province' : province,
            'district' : district,
            'confidence' : confidence,
        }
        """
        return {
            'province': province,
            'district': district,
            'confidence': confidence
        }

    def map_pair_norm(self, pair: dict):
        """
        Normlize thông tin địa chỉ lại
        :param pair:
        :return:
        """
        if pair is None:
            return pair
        if pair.get("province") is not None:
            if self.province2province.get(pair.get("province")) is None:
                print("Final Province ERROR province2province: {}".format(pair.get("province")))
            else:
                pair["province"] = self.province2province.get(pair.get("province"))
        return pair

    def fill_entity_miss(self, pair: dict) -> Dict:
        """
        Tìm và điền các thông tin district và province nếu còn thiếu
        :param pair:
        :return:
        """
        if pair is None:
            return None
        if pair.get("province") is None:
            pair["province"] = self.find_province_miss(pair.get("district"), pair.get("ward"))
        if pair.get("district") is None:
            pair["district"] = self.find_district_miss(pair.get("province"), pair.get("ward"))
        return pair


class ExtractorRecordAddress(ExtractorEntityAddress):
    def process(self, message, status_add_symbol=False) -> Dict:
        """
        Trích rút thông tin địa chỉ theo content, district, province.
        :param message: text chứa thông tin địa chỉ cần trích rút
        :param status_add_symbol: Trạng thái có thêm dấu ',' giữa (đường,phưỡng,xã,phố,khu,...) phục vụ cho phần TTS
        :return (dict): thông tin sau khi trích rút
            {
                "content": ..., ## các nội dụng còn lại ngoài district,province
                "district": ..., ## tên quận,huyện,thị xã
                "province": ..., ## tên tỉnh,thành phố
            }
        """
        list_entity_all = self.regex_process_entity.get_extract_entities(message,
                                                                         self.regex_process_entity.regex_feature.get(
                                                                             "ObjectAddress"), status_space=True)
        list_entity = self.regex_process_entity.remove_entites_overlap(list_entity_all)  ## Remove overlap entity
        keyword_address = ["keyword_street", "keyword_ward", "keyword_district",
                           "keyword_province"]  ## danh sách keyword name_entity
        list_entity = self.process_confidence_district_and_province(list_entity, keyword_address)
        list_district_province = self.select_list_district_and_province(
            list_entity)  ## Danh sách các cặp district, province
        ## Thực hiện chọn lựa cặp có confidence cao nhất.
        if list_district_province is None or len(list_district_province) == 0:
            district_province = {}
        else:
            list_district_province = sorted(list_district_province, key=lambda x: x['confidence'], reverse=True)
            district_province = list_district_province[0]
            if district_province.get("province") is None:
                district_province["province"] = self.get_province_from_district(district_province.get("district"))

        ## Thực hiện chọn đoạn content hợp lý
        content, confidence_content = self.select_content(message, list_entity)
        if district_province.get("district") is None and district_province.get("province") is None and (
                confidence_content is None or confidence_content < 0.5):
            content = None
        content = self.replace_keyword_address_repate(content)
        if self.check_is_number_text(content) == True:
            content = None
        if status_add_symbol == True:
            content = self.add_symbol_comma(content)
        district_province["content"] = content
        return district_province

    def process_confidence_district_and_province(self, list_entity: List[dict], keyword_address: List[str]) -> List[
        dict]:
        """
        Thực hiện kiểm tra và set confidence entity district, province sau khi trích rút.
        Giải quyết vấn đề tên tỉnh, tên huyện, tên xã, tên đường phố giống nhau.
        :param list_entity: Danh sách các entity trích rút
        :param keyword_address: Danh sách tên các keyword address
        :return:
        """
        if list_entity is None or len(list_entity) == 0:
            return []
        list_entity = sorted(list_entity, key=lambda x: x['start'], reverse=True)
        list_entity_keyword_address = []  ## Danh sách các entity có name_entity trong keyword_address
        for i, entity in enumerate(list_entity):
            if entity.get("entity") in keyword_address:
                list_entity_keyword_address.append(entity)

        ## Thực hiện kiểm tra và gán lại entity
        for i, entity in enumerate(list_entity):
            if entity.get("entity") == "district":
                confidence, name_entity = self.find_name_entity_and_confidence_from_district(entity,
                                                                                             list_entity_keyword_address)
                entity["entity"] = name_entity
                entity["confidence"] = confidence
            if entity.get("entity") == "province":
                confidence, name_entity = self.find_name_entity_and_confidence_from_province(entity,
                                                                                             list_entity_keyword_address)
                entity["entity"] = name_entity
                entity["confidence"] = confidence
        return list_entity

    def select_list_district_and_province(self, list_entity: List[dict]) -> List[dict]:
        """
        Hàm này dùng để tính toàn các cặp district và province với hệ số đánh giá (coefficient) để tính ra confidence.
        confidence = coefficient * confidence_of_district * confidence_of_province.
        coefficient được quy ước như sau:
            coefficient = 0.5: district, province xung đột không đồng nhất trên CSDL
            coefficient = 1: district or province is None
            coefficient = 4: district, province đồng nhất trên CSDL
        :param list_entity:
        :return:
        elements: (List[dict]) Danh sách các cặp district và province sau khi tính toán
        """
        extractor_name = {}
        list_entity = sorted(list_entity, key=lambda x: x['confidence'], reverse=True)
        for entity in list_entity:
            if entity.get("entity") in ["district", "province"]:
                if extractor_name.get(entity.get("entity")) is None:
                    extractor_name[entity.get("entity")] = []
                extractor_name[entity.get("entity")].append(entity)

        elements = []
        if extractor_name.get("province") is not None and len(extractor_name.get("province")) > 0:
            for entity_province in extractor_name.get("province"):
                if extractor_name.get("district") is None or len(extractor_name.get("district")) == 0:
                    elements.append(self.get_dict_district_and_province(entity_province.get("value"), None,
                                                                        1 * entity_province.get("confidence")))
                    continue
                for entity_district in extractor_name.get("district"):
                    confidence = 4 if self.check_district_province(entity_province.get("value"),
                                                                   entity_district.get("value")) == True else 0.5
                    confidence = confidence * entity_province.get("confidence") * entity_district.get("confidence")
                    elements.append(
                        self.get_dict_district_and_province(entity_province.get("value"), entity_district.get("value"),
                                                            confidence))
            return elements

        if extractor_name.get("district") is not None and len(extractor_name.get("district")) > 0:
            for entity_district in extractor_name.get("district"):
                elements.append(self.get_dict_district_and_province(None, entity_district.get("value"),
                                                                    1 * entity_district.get("confidence")))
        return elements

    def select_content(self, message: str, list_entity) -> str:
        """
        Hàm này cho phép trích rút chọn được vùng chứa content.
        :param message: Text địa chỉ cần trích rút
        :param list_entity: Array entity district,province và các entity khác.
        :return:
        """
        list_content = []
        list_entity = sorted(list_entity, key=lambda x: x['start'])
        index = 0
        name_entity_split = ["district", "province", "keyword_province", "keyword_district"]
        for entity in list_entity:
            if entity.get("entity") in name_entity_split:
                if index < entity.get("start") and len(message[index:entity.get("start")].strip()) > 3:
                    list_content.append(message[index:entity.get("start")].strip())
                    index = entity.get("end")
                else:
                    index = entity.get("end")
        if index < len(message) and len(message[index:].strip()) > 3:
            list_content.append(message[index:].strip())
        # print ("list_content = ", list_content)

        ## từ các vùng content nguyên thủy đưa để tính toán độ tin cậy và lựa chọn lại vùng content hợp lý
        candidate_content = []
        for content in list_content:
            content, confidence = self.regex_content(content)
            if self.ward2ward.get(content.strip()) is not None:
                confidence = 1
                content = content.strip()
            # print (f"content = {content}, confidence = {confidence}")
            content = self.select_best_text(content, reverse=True)
            content = self.norm_text(content)
            if len(content.strip()) < 3 or len(content.strip().split(" ")) < 2:
                continue
            candidate_content.append({
                'content': content,
                'confidence': confidence,
            })
        if candidate_content is None or len(candidate_content) == 0:
            return None, None
        candidate_content = sorted(candidate_content, key=lambda x: x['confidence'], reverse=True)
        max_confidence = candidate_content[0].get('confidence')
        content_best = ""
        for candidate in candidate_content:
            if max_confidence == candidate.get('confidence'):
                if len(content_best) < len(candidate.get('content')):
                    content_best = candidate.get('content')
            else:
                break
        return content_best, max_confidence

    def regex_content(self, content: str):
        """
        Tính toán độ tin cậy của content và chọn lại vùng content hợp lý sau khi regex
        :param content: text chứa content
        :return:
            content: content sau khi regex
            confidence: độ tin cậy của content
        """
        list_entity_content_all = self.regex_process_entity.get_extract_entities(content,
                                                                                 self.regex_process_entity.regex_feature.get(
                                                                                     "Content"), status_space=True)
        list_entity_content = self.regex_process_entity.remove_entites_overlap(
            list_entity_content_all)  ## Remove overlap entity
        content_remove_stop = self.regex_process_entity.remove_stop_word_from_mesage(content, list_entity_content)

        entity_from_end = []
        for entity in list_entity_content:
            if entity.get("entity") == "keyword_end":
                entity_from_end.append(entity)
        content_remove_stop = self.regex_process_entity.remove_text_from_entities(content_remove_stop, entity_from_end)
        list_entity_content = sorted(list_entity_content, key=lambda i: i['start'])
        index_start = 100000
        index_end = 100000
        for entity in list_entity_content:
            if entity.get("entity") == "keyword_end":
                index_end = entity.get("end")
            if entity.get("entity") == "keyword_start":
                index_start = min(index_start, entity.get("start"))
        index = min(index_start, index_end)
        if index == 100000:
            return content_remove_stop, 0
        if self.check_is_number_text(content_remove_stop[index:].strip()) == True:
            return content_remove_stop[index:].strip(), 0
        confidence = self.get_confidence_content(list_entity_content)
        return content_remove_stop[index:].strip(), confidence

    def get_confidence_content(self, list_entity_content: List[str]) -> float:
        """
        Tính toán confidence content dựa vào entity keyword_start
        :param list_entity_content: Danh sách các entity content được trích rút
        :return:
            (float): Độ tin cậy của content
        """
        number_keyword = 0
        for entity in list_entity_content:
            if entity.get("entity") == "keyword_start" and self.check_is_number_text(entity.get("value")) == False:
                number_keyword += 1

        if number_keyword == 0:
            return 0.5
        if number_keyword == 1:
            return 1
        if number_keyword > 1:
            return 2

    def find_name_entity_and_confidence_from_district(self, entity_district: dict,
                                                      list_entity_keyword_address) -> Tuple:
        """
        Kiểm tra thông tin entity district và tính toán confidence của district
        :param entity_district: entity district đã trích rút
        :param list_entity_keyword_address: Danh sách cách entity của keyword_address
        :return: Trả về confidence và name_entity
        """
        if entity_district.get("value").find("quận ") != -1:
            return CONFIDENCE_CORRECT_ENTITY, "district"
        if list_entity_keyword_address is None or len(list_entity_keyword_address) == 0:
            return CONFIDENCE_NORMAL_ENTITY, "district"
        for entity in list_entity_keyword_address:
            if entity.get("end") + 1 == entity_district.get("start"):
                if entity.get("entity") == "keyword_district" or entity.get("value") == "thành phố":
                    return CONFIDENCE_CORRECT_ENTITY, "district"
                else:
                    if entity.get("entity") == "keyword_province":
                        if self.district_like_province.get(entity_district.get("value")) == True:
                            return CONFIDENCE_CORRECT_ENTITY, "province"
                        else:
                            return CONFIDENCE_WRONG_ENTITY, "district"
                    if entity.get("entity") == "keyword_ward":
                        return CONFIDENCE_ZERO_ENTITY, "ward"
                    return CONFIDENCE_ZERO_ENTITY, "content"
        return CONFIDENCE_NORMAL_ENTITY, "district"

    def find_name_entity_and_confidence_from_province(self, entity_province: dict,
                                                      list_entity_keyword_address) -> Tuple:
        """
        Kiểm tra thông tin entity province và tính toán confidence của province
        :param entity_province: entity province đã trích rút
        :param list_entity_keyword_address: Danh sách cách entity của keyword_address
        :return: Trả về confidence và name_entity
        """
        if list_entity_keyword_address is None or len(list_entity_keyword_address) == 0:
            return CONFIDENCE_NORMAL_ENTITY, "province"
        for entity in list_entity_keyword_address:
            if entity.get("end") + 1 == entity_province.get("start"):
                if entity.get("entity") == "keyword_province":
                    if entity.get("value") == "thành phố" and entity_province.get(
                            "value") not in self.province_tp and self.district_like_province.get(
                        entity_province.get("value")) == True:
                        return CONFIDENCE_CORRECT_ENTITY, "district"
                    return CONFIDENCE_CORRECT_ENTITY, "province"
                else:
                    if entity.get("entity") == "keyword_district":
                        if self.district_like_province.get(entity_province.get("value")) == True:
                            return CONFIDENCE_CORRECT_ENTITY, "district"
                        else:
                            return CONFIDENCE_WRONG_ENTITY, "province"
                    return CONFIDENCE_ZERO_ENTITY, "content"
        return CONFIDENCE_NORMAL_ENTITY, "province"

    def get_dict_district_and_province(self, province: str, district: str, confidence: float) -> Dict:
        """
        Tạo một dict district province
        :param province:
        :param district:
        :param confidence:
        :return:
        """
        return {
            'province': province,
            'district': district,
            'confidence': confidence
        }

    def add_symbol_comma(self, text: str):
        """
        Thêm dấu comma vào trong text trước vị trị xuất hiện của keyword_address
        :param text: đầu vào text
        :return:
        output (str): text sau khi thêm comma
        """
        if text is None or len(text) == 0:
            return text
        list_entity_comma_all = self.regex_process_entity.get_extract_entities(text,
                                                                               self.regex_process_entity.regex_feature.get(
                                                                                   "SplitClusterContent"),
                                                                               status_space=True)
        list_entity_comma = self.regex_process_entity.remove_entites_overlap(
            list_entity_comma_all)  ## Remove overlap entity
        list_entity_comma = sorted(list_entity_comma, key=lambda i: i['start'], reverse=True)
        output = text
        for entity in list_entity_comma:
            if entity.get("entity") == "keyword_start":
                if entity.get("start") == 0:
                    output = output[entity.get("start"):]
                else:
                    output = output[:entity.get("start")] + ", " + output[entity.get("start"):]
        return output

    def replace_keyword_address_repate(self, content: str):
        """
        Loại bỏ các keyword address bị lặp. như phường phường -> phường  / đường phường -> đường ...
        :param content:
        :return:
        """
        if content is None or len(content) == 0:
            return content
        list_entity_all = self.regex_process_entity.get_extract_entities(content,
                                                                         self.regex_process_entity.regex_feature.get(
                                                                             "NormContent"), status_space=True)
        list_entity = self.regex_process_entity.remove_entites_overlap(list_entity_all)  ## Remove overlap entity
        if len(list_entity) == 0:
            return content
        list_entity = sorted(list_entity, key=lambda i: i['start'])
        output = ""
        start = 0
        for entity in list_entity:
            output += content[start:entity.get("start")] + entity.get("value").split(" ")[-1]
            start = entity.get("end")
        output += content[start:]
        return output.strip()

    def get_province_from_district(self, district: str):
        """
        Hàm này cho phép lấy thông tin province khi biết được district.
        :param district: Tên quận,huyện
        :return:
            province (str): tên thảnh phố,tỉnh
        """
        if self.district2district.get(district) is not None and len(
                self.district2province.get(self.district2district.get(district))) == 1:
            return self.district2province.get(self.district2district.get(district))[0]
        return None

    def select_best_text(self, text, reverse=False):
        """
        Lựa chọn đoạn text
        :param text:
        :param reverse:
        :return:
        """
        if text is None:
            return None
        if reverse == True:
            text = text[::-1]
        index = text.find(" " * 4)
        output = None
        if index == -1:
            output = text
        else:
            output = text[:index]
        if reverse == True:
            output = output[::-1]
        return output


# if __name__ == '__main__':
#     from regex_process_entity import RegexProcessEntity
#     from norm_number import NormalizeNumber
#
#     regex_process_entity = RegexProcessEntity("/home/smartcall/smartcall-projects/1.TemplateProject/smartcall-1-template-project-ver3-master/resources/nlu/ner/regex_address/regex_feature_address.json", "/home/smartcall/smartcall-projects/1.TemplateProject/smartcall-1-template-project-ver3-master/resources/nlu/ner/regex_address/")
#     with open("/home/smartcall/smartcall-projects/1.TemplateProject/smartcall-1-template-project-ver3-master/resources/nlu/ner/regex_address/text2number.json", "r", encoding='utf-8') as file:
#         text2number = json.load(file)
#     norm_number = NormalizeNumber(regex_feature=regex_process_entity, text2number=text2number)
#     extrator_layer_address = ExtractorLayerAddress(regex_process_entity, norm_number,
#                                                    dir_address="/home/smartcall/smartcall-projects/1.TemplateProject/smartcall-1-template-project-ver3-master/resources/nlu/ner/regex_address/")
#     list_messsage = [
#         "anh ở văn khê huyện mê linh",
#         "ở cư tránh số hai tên là tên thủy bằng huyện cam ranh à à ừ gọi là đà nẵng",
#         "số 33 ngõ 2 hàm nghi mỹ đình 1 hà nội",
#         "ừ ở xã tân an thị xã tam hiệp huyện tỉnh kiên giang",
#         "phường trường thanh quận chín chị",
#         "số 1 xã mai dịch",
#         "lê đưc thọ hà nội",
#     ]
#     for message in list_messsage:
#         print("======message = ", message)
#         output = extrator_layer_address.process(message, status_regex_all_ward=True)
#         print(output)
#         print("=============================")
