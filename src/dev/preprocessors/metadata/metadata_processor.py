from src.core.utils.classes import classes_enumeration
from src.dev.preprocessors.metadata.modules import (
    NERPersonalName,
    NERAddress,
    DatetimeExtractor
)
from src.core.api_metadata import api_metadata_classes as ac


class PreprocessingExternalMetadata:
    def __init__(self):
        self.data = {}
        self.modules = [
            # NERPersonalName(),
            # NERAddress(),
            # DatetimeExtractor()
        ]
        api_classes_class, _ = classes_enumeration.get_classes(ac.__name__)
        api_objects = [clazz() for clazz in api_classes_class]
        api_objects_name = [obj.name() for obj in api_objects]
        for idx, name in enumerate(api_objects_name):
            self.modules.append(api_objects[idx])

    async def process(self, message, recorder=None):
        for module in self.modules:
            processed_data = await module.process(message, recorder)
            update_data = {
                module.name: processed_data
            }
            self.data.update(
                update_data
            )

    def get_metadata(self):
        return self.data
