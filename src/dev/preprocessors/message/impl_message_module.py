class ImplMessageModule:
    def __init__(self):
        pass

    def process(self, message):
        raise NotImplementedError("An ImplMessageModule must implement its process method")
