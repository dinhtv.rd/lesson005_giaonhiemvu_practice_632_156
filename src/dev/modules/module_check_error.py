import re, os, json
from typing import List, Dict
import logging, copy
import traceback

from src.dev.modules.define import PATH_DATA_GRAMMAR, PATH_DATA_WORDING, PATH_DATA_FLUENCY

logger = logging.getLogger(__name__)


class ModuleCheckERROR:
    def __init__(self):
        self.data_grammar = []  ## List[dict]
        if os.path.isfile(PATH_DATA_GRAMMAR):
            with open(PATH_DATA_GRAMMAR, "r") as file:
                self.data_grammar = json.load(file)

        self.data_wording = {}  ## Dict[list[dict]]
        if os.path.isfile(PATH_DATA_WORDING):
            with open(PATH_DATA_WORDING, "r") as file:
                self.data_wording = json.load(file)

        self.data_fluency = {}  ## Dict[list[dict]]
        if os.path.isfile(PATH_DATA_FLUENCY):
            with open(PATH_DATA_FLUENCY, "r") as file:
                self.data_fluency = json.load(file)

    def process_check_wording(self, message: str, id_question: str, type_error: str = "wording_error") -> List[dict]:
        """
        Thực hiện check lỗi diễn đạt
        :param message: text of user talk
        :param id_question: ID of question which ask user answer
        :return:
        [
            {
                "phrase_wrong": str, ## cụm từ sai
                "phrase_correct": str, ## cụm từ đúng
                "guide": str, ## Câu giải thích
                "question": str,  ## textbot, ## Last
                "answer": str,  ## Textuser,
                "type_error": str, ## tên lỗi
            }
        ]
        """
        try:
            if id_question is None:
                return []
            result_error_wording = []
            id_question = id_question.lower()
            if self.data_wording.get(id_question) is None:
                return None
            for element in self.data_wording.get(id_question):
                logger.debug("element: {element}")
                phrase_wrong = element.get("phrase_wrong").lower()
                phrase_correct = element.get("phrase_correct").lower()
                if re.match(pattern=f".*\\b({phrase_wrong})\\b.*", string=message.lower(),
                            flags=re.IGNORECASE) and not re.match(pattern=f".*\\b({phrase_correct})\\b.*",
                                                                  string=message, flags=re.IGNORECASE):
                    output = {
                        "phrase_wrong": phrase_wrong,
                        "phrase_correct": phrase_correct,
                        "guide": element.get("guide"),
                        "question": element.get("question"),  ## textbot, ## Last
                        "answer": message,  ## Textuser,
                        "type_error": type_error,
                    }
                    result_error_wording.append(copy.deepcopy(output))
            return result_error_wording
        except Exception as e:
            logger.debug(f"ERROR Process Wording: {traceback.format_exc()}")
            return []

    def process_check_grammar(self, message: str, type_error="grammar_error") -> List[dict]:
        """
        Thực hiện check lỗi ngữ pháp
        :param message: text user trả lời
        :return:
        [
            {
                "phrase_wrong": phrase_wrong, ## cụm từ sai
                "phrase_correct": phrase_correct, ## cụm từ đúng
                "guide": str, ## Câu giải thích
                "answer": message,  ## Text user trả lời
                "type_error": type_error ## loại lỗi
            }
        ]
        """
        try:
            if not isinstance(self.data_grammar, list):
                return None
            result_error_grammar = []
            for element in self.data_grammar:
                phrase_wrong = element.get("phrase_wrong").lower()
                phrase_correct = element.get("phrase_correct").lower()
                if re.match(pattern=f".*\\b({phrase_wrong})\\b.*", string=message.lower(), flags=re.IGNORECASE):
                    output = {
                        "phrase_wrong": phrase_wrong,
                        "phrase_correct": phrase_correct,
                        "guide": element.get("guide"),
                        "answer": message,
                        "type_error": type_error
                    }
                    result_error_grammar.append(copy.deepcopy(output))
            return result_error_grammar
        except Exception as e:
            logger.debug(f"ERROR Process grammar: {traceback.format_exc()}")
            return []

    def process_check_fluency(self, message: str, id_question: str, phrase_wrong: str, type_error="fluency_error") -> \
    List[dict]:
        """
        Thực hiện check lỗi trôi chảy
        :param message: text user trả lời
        :param question: text câu hỏi
        :param phrase_wrong: text Câu trả lời chưa đầy đủ ý (trên DB)
        :param type_error: loại lỗi
        :return:
        [
            {
                "phrase_wrong": str, ## Câu nói chưa trôi chảy
                "phrase_correct": str, ## câu nói cần trôi chảy
                "question": str,  ## text câu hỏi
                "answer": str,  ## Text user trả lời
                "type_error": str, ## loại lỗi
            }
        ]
        """
        try:
            if id_question is None or phrase_wrong is None:
                return []
            result_error_fluency = []
            id_question = id_question.lower()
            if self.data_fluency.get(id_question) is None:
                return None
            for element in self.data_fluency.get(id_question):
                if phrase_wrong.lower() == element.get("phrase_wrong").lower():
                    output = {
                        "phrase_wrong": message,
                        "phrase_correct": element.get("phrase_correct"),
                        "question": element.get("question"),  ## textbot, ## Last
                        "answer": message,  ## Textuser,
                        "type_error": type_error,
                    }
                    result_error_fluency.append(copy.deepcopy(output))
            return result_error_fluency
        except Exception as e:
            logger.debug(f"ERROR Process fluency: {traceback.format_exc()}")
            return []
