from src.core.condition import condition_fixed as cf
from src.dev.context import custom_condition_classes as ccc, condition_classes as cc
from src.core.utils.classes import classes_enumeration

CONFIG_RECORDS = {}


def get_records(class_name):
    answer = {}
    condition_objects, condition_objects_name, condition_classes_class = \
        classes_enumeration.extract_python_classes(class_name)
    for idx, name in enumerate(condition_objects_name):
        answer[name] = condition_objects[idx]
    return answer


CONFIG_RECORDS.update(get_records(cc.__name__))
CONFIG_RECORDS.update(get_records(cf.__name__))
CONFIG_RECORDS.update(get_records(ccc.__name__))

CONFIG_NAME_QUERY_RECORDS = [
]
