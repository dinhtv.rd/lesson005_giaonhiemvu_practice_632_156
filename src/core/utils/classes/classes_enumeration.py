import sys
import inspect
import copy


def get_classes(module_name):
    classes = []
    classes_source = []
    module = sys.modules[module_name]
    for name, obj in inspect.getmembers(module):
        if inspect.isclass(obj) and inspect.getmodule(obj) == module:
            classes.append(obj)
            classes_source.append(inspect.getsource(obj))
    return classes, classes_source


def extract_python_classes(class_name):
    condition_classes_class, condition_classes_source = get_classes(class_name)
    condition_objects = [clazz() for clazz in condition_classes_class]
    condition_objects_name = [obj.name() for obj in condition_objects]

    return copy.deepcopy(condition_objects), \
           copy.deepcopy(condition_objects_name), \
           copy.deepcopy(condition_classes_source)
