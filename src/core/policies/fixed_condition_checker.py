import re
import logging

from config_platform import (
    FIXED_CONDITIONS,
    FIXED_UPDATE_CONDITIONS,
    FIXED_CHECK_CONDITIONS
)

logger = logging.getLogger(__name__)


def get_condition_data(desc):
    tmp = desc.split(":", 1)
    if len(tmp) != 2:
        args = None
    else:
        desc, args = tmp[0], tmp[1]
    return desc, args


def check_other_fixed_condition(desc, recorder):
    desc = desc.lower()
    if desc in FIXED_UPDATE_CONDITIONS:
        return True
    if desc in FIXED_CHECK_CONDITIONS:
        return recorder.get_current_record().get(desc)
    return False


def check_repeat_condition(recorder, repeat_count):
    trace_nodes = recorder.trace_nodes
    logger.debug(f"TRACE NODES REPEAT: {trace_nodes}")
    logger.debug(f"TRACE NODES REPEAT: {recorder.get_current_record().get('repeat_count')}")
    if trace_nodes:
        cur_node = trace_nodes[-1]
    else:
        return False
    return recorder.get_current_record().get("repeat_count").get(cur_node) == repeat_count


def check_remain_condition():
    return True


def remove_condition_id(desc):
    if not desc:
        return desc
    desc = re.sub(r"^\[\d+\]", "", desc, 1)
    return desc.strip()


def check(desc, recorder):
    desc = remove_condition_id(desc)
    condition_id, _ = get_condition_data(desc)
    if condition_id in ["Remain", "remain"]:
        return check_remain_condition()

    if condition_id in ["Checkpoint", "checkpoint"]:
        return True

    if condition_id.lower() in FIXED_CONDITIONS:
        return check_other_fixed_condition(condition_id, recorder)

    try:
        repeat_count = int(condition_id)
        return check_repeat_condition(recorder, repeat_count)
    except Exception as e:
        print(e)
        return False
