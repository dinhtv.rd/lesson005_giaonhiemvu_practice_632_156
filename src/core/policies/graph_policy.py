import logging
from typing import Optional, Any, Dict, List, Text

from rasa.shared.core.domain import Domain
from rasa.shared.nlu.interpreter import NaturalLanguageInterpreter
from rasa.core.policies.policy import Policy, PolicyPrediction
from rasa.shared.core.trackers import DialogueStateTracker
from rasa.shared.core.generator import TrackerWithCachedStates
from rasa.core.constants import MEMOIZATION_POLICY_PRIORITY

import numpy as np

from src.core.policies.flow_selector import FlowSelector
from src.core.policies.graph_loader import GraphLoader
from src.core.nlu.classifiers.regex_classifier import RegexIntentClassifier
from src.dev.context.const.recorder import Recorder
from src.core.database.context_store import RedisStore, ContextStore
from src.core.utils.io import jsonio

from config import (
    PATH_CONFIG_STOP_WORDS,
    MAX_INTENTS,
    CONFIG_DOMAIN,
    DOMAIN_FINISH_ACTIONS,
    DOMAIN_INTENTS,
    DEFAULT_THRESHOLD,
    DEFAULT_THRESHOLD_SHORT_MESSAGE,
    DEFAULT_FALLBACK_INTENTS
)

logger = logging.getLogger(__name__)


class GraphPolicy(Policy):
    ENABLE_FEATURE_STRING_COMPRESSION = True

    USE_NLU_CONFIDENCE_AS_SCORE = False

    def __init__(
            self,
            priority: int = MEMOIZATION_POLICY_PRIORITY,
            lookup: Optional[Dict] = None,
            confidence_threshold: Optional[float] = DEFAULT_THRESHOLD,
            confidence_threshold_short_message: Optional[float] = DEFAULT_THRESHOLD_SHORT_MESSAGE,
            fallback_intents: Optional[List] = DEFAULT_FALLBACK_INTENTS,
            break_fallback_again=True,
            fixed_intents=None,
            **kwargs: Any,
    ) -> None:
        """Initialize the policy.

        Args:
            priority: the priority of the policy
        """
        super().__init__(priority=priority, **kwargs)
        self.lookup = lookup if lookup is not None else {}

        self.confidence_threshold = DEFAULT_THRESHOLD if not confidence_threshold else confidence_threshold
        self.confidence_threshold_short_message = DEFAULT_THRESHOLD_SHORT_MESSAGE if \
            not confidence_threshold_short_message else confidence_threshold_short_message

        with open(PATH_CONFIG_STOP_WORDS, "r") as stop_words:
            self.stop_words = jsonio.load_utf8(stop_words)
        self.regex_classifier = RegexIntentClassifier()
        self.graph_controller = GraphLoader()
        self.flow_selector = FlowSelector()
        self.redis_more_store = RedisStore()

        metadata = self._metadata()
        logger.info(f"Graph Policy args: {metadata}")

    def train(
            self,
            training_trackers: List[TrackerWithCachedStates],
            domain: Domain,
            interpreter: NaturalLanguageInterpreter,
            **kwargs: Any,
    ) -> None:
        pass

    def change_confidence_by_custom_model(
            self,
            tracker: DialogueStateTracker,
            recorder: Recorder) -> None:
        sender_id = tracker.sender_id
        real_intent = {"name": "", "confidence": 0}

        logger.info(f"[GRAPH POLICY][{sender_id}] - Intent after NLU: {tracker.latest_message.parse_data['intent_ranking']}")
        message = tracker.latest_message.parse_data["text"]

        for intent_name in CONFIG_DOMAIN.get(DOMAIN_INTENTS):
            check_in = False
            for i, intent_tmp in enumerate(tracker.latest_message.parse_data["intent_ranking"]):
                if intent_tmp["name"] == intent_name:
                    check_in = True
                    break
            if not check_in:
                tracker.latest_message.parse_data["intent_ranking"]. \
                    append({"name": intent_name, "confidence": 0.0})

        state_regex = MAX_INTENTS
        for i, intent_tmp in enumerate(tracker.latest_message.parse_data["intent_ranking"]):
            label_regex, idx_regex = self.regex_classifier.predict_name_intent(message, intent_tmp["name"])
            if label_regex is not None and state_regex > idx_regex:
                tracker.latest_message.parse_data["intent_ranking"][i]["confidence"] = 1.0
                real_intent["confidence"] = 1.0
                real_intent["name"] = intent_tmp["name"]
                state_regex = idx_regex
            else:
                if real_intent["confidence"] < intent_tmp["confidence"]:
                    real_intent = intent_tmp
                else:
                    tracker.latest_message.parse_data["intent_ranking"][i]["confidence"] = 0.0

        logger.info(f"[GRAPH POLICY][{sender_id}] - Intent after Regex: {real_intent}")
        tracker.latest_message.parse_data["intent"].update(real_intent)

    def change_confidence_by_fallback_policy(
            self,
            tracker: DialogueStateTracker
    ) -> None:
        # Process intent fallback
        sender_id = tracker.sender_id
        real_intent = tracker.latest_message.parse_data["intent"].copy()
        message = tracker.latest_message.parse_data["text"]
        threshold = self.confidence_threshold
        words = message.strip().split(" ")
        len_words = 0
        for w in words:
            if w not in self.stop_words:
                len_words += 1
        is_short_message = len_words <= 1
        if is_short_message:
            threshold = self.confidence_threshold_short_message
        if real_intent["confidence"] < threshold:
            real_intent["name"] = "intent_fallback"
            real_intent["confidence"] = 1.0
            for i, intent_tmp in enumerate(tracker.latest_message.parse_data["intent_ranking"]):
                if intent_tmp["name"] == real_intent["name"]:
                    tracker.latest_message.parse_data["intent_ranking"][i]["confidence"] = 1.0
                else:
                    tracker.latest_message.parse_data["intent_ranking"][i]["confidence"] = 0.0

        tracker.latest_message.parse_data["intent"].update(real_intent)
        logger.info(f"[GRAPH POLICY][{sender_id}] - Intent after Fallback: {real_intent}")

    def predict_action_probabilities(
            self,
            tracker: DialogueStateTracker,
            domain: Domain,
            interpreter: NaturalLanguageInterpreter,
            **kwargs: Any,
    ) -> PolicyPrediction:
        """Predicts the next action the bot should take
            after seeing the tracker.

            Returns the list of probabilities for the next actions.
            If memorized action was found returns 1.1 for its index,
            else returns 0.0 for all actions."""
        result = [0.0] * domain.num_actions
        sender_id = tracker.sender_id  # get id of user send message
        recorder = self.redis_more_store.get_data(sender_id).tickets["recorder"]
        model_intent = None
        if recorder.state is True and not tracker.latest_message.parse_data["text"].startswith("/"):
            model_intent = tracker.latest_message.parse_data["intent"].copy()
            self.change_confidence_by_custom_model(tracker, recorder)
            self.change_confidence_by_fallback_policy(tracker)
            # state of set up parameter of intent rank
        cur_intent = tracker.latest_message.parse_data["intent"]["name"]
        pre_action = recorder.cur_action
        if recorder.cur_action is None:
            pre_action = self.graph_controller.start_node
        if recorder.state is True:
            predicted_action, recorder = self.flow_selector.select_flow(
                tracker=tracker,
                pre_action=pre_action,
                cur_intent=cur_intent,
                recorder=recorder,
                model_intent=model_intent
            )
            if predicted_action is None:
                predicted_action = "action_quit"
            index_action = domain.action_names_or_texts.index(predicted_action)
            result[index_action] = 1.0
        else:
            index_action = domain.action_names_or_texts.index("action_listen")
            result[index_action] = 1.0

        result_np = result
        index = np.where(result_np)[0]
        check_end_conversation = False
        if index is not None and len(index) > 0:
            index = index[0].tolist()
            next_action = domain.action_names_or_texts[index]
            if next_action in domain.user_actions:
                if self.graph_controller.is_finish_action(next_action):
                    check_end_conversation = True
                else:
                    recorder.state = False
            else:
                recorder.state = True
        else:
            check_end_conversation = True
        if check_end_conversation:
            logger.info(f"[GRAPH POLICY][{tracker.sender_id}] - Finish conversation!")
        self.redis_more_store.save_data(ContextStore(sender_id, recorder=recorder))
        logger.info(f"[GRAPH POLICY][{tracker.sender_id}] - Save record data to Redis DB - state {recorder.state}")
        recorder.update_report_to_db()
        return self._prediction(result)

    def _metadata(self) -> Dict[Text, Any]:
        metadata = {
            "priority": self.priority,
            "confidence_threshold": self.confidence_threshold,
            "confidence_threshold_short_message": self.confidence_threshold_short_message,
        }
        return metadata

    @classmethod
    def _metadata_filename(cls) -> Text:
        return "graph_policy.json"
