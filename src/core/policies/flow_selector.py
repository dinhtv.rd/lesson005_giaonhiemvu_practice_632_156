import copy

from src.core.policies.graph_loader import GraphLoader
from src.core.initializer.drawio_extractor import DrawioExtractor
from src.core.policies.fixed_condition_checker import check, remove_condition_id
from config_platform import (
    VERTICES,
    FLOWS,
    FIXED_CONDITIONS
)
import logging

logger = logging.getLogger(__name__)


class FlowSelector(object):
    def __init__(self):
        self.graph_controller = GraphLoader()
        pass

    def find_next_flow(self, pre_node, cur_intent, recorder, trace, cnt_message):
        trace_old = copy.deepcopy(trace)
        recorder_old = copy.deepcopy(recorder)

        logger.info(
            f"[FLOW SELECTOR][{recorder.sender_id}] Find path from node - {pre_node}: "
            f"{self.graph_controller.get_node(pre_node).get('desc')}")

        adjacency_nodes = self.graph_controller.get_adjacency_nodes(pre_node)
        pre_node_type = self.graph_controller.get_node(pre_node).get("type_node")
        if pre_node_type in VERTICES:
            for node_id in adjacency_nodes:
                node = self.graph_controller.get_node(node_id)
                type_node = node.get("type_node")
                if type_node in ["intent_flow", "update_local"]:
                    cnt_message += 1
                    break
            if cnt_message == 2 or pre_node_type == "end_node":
                return pre_node, trace, recorder

        for flow_node_id in adjacency_nodes:
            node = self.graph_controller.get_node(flow_node_id)
            logger.info(f"[FLOW SELECTOR][{recorder.sender_id}] Find path from node - {pre_node}: "
                        f"++++++ Adjacency node {flow_node_id}: {node.get('desc')}")

        for flow_node_id in adjacency_nodes:
            node = self.graph_controller.get_node(flow_node_id)
            type_node = node.get("type_node")
            check_flow = False
            if type_node == "condition_flow":
                check_all_conditions = True
                if node.get("params").get("check"):
                    for condition in node.get("params").get("check"):
                        condition_id = DrawioExtractor.extract_condition_code(condition)
                        recorder.update_by_condition_id(condition_id)
                        if not recorder.get_current_record().get(condition_id):
                            check_all_conditions = False
                if check_all_conditions:
                    if node.get("params").get("update"):
                        for condition in node.get("params").get("update"):
                            condition_id = DrawioExtractor.extract_condition_code(condition)
                            recorder.update_by_condition_id(condition_id)
                    check_flow = True
            elif type_node == "intent_flow":
                check_flow = False
                for intent_name in node.get("params"):
                    if intent_name == cur_intent:
                        check_flow = True
                    if intent_name == "intent_fallback":
                        check_flow = True
            elif type_node == "update_local":
                for condition in node.get("params"):
                    condition_id = DrawioExtractor.extract_condition_code(condition)
                    recorder.update_by_condition_id(condition_id)
                check_flow = True
            elif type_node == "fixed_condition":
                desc = node.get("desc")
                desc = remove_condition_id(desc)
                condition_id, args = self.get_condition_data(desc)
                if condition_id in FIXED_CONDITIONS:
                    recorder.update_by_condition_id(condition_id, args)
                check_flow = check(condition_id, recorder)
            else:
                check_flow = True

            logger.info(f"[FLOW SELECTOR][{recorder.sender_id}] Find path from node - {pre_node}: "
                        f"=====> Try to follow adjacency node {flow_node_id}: {node.get('desc')}"
                        f" => Result: {'SUCCESS' if check_flow else 'FAIL'}")
            if check_flow:
                recorder.update_trace_node(flow_node_id)
                trace[flow_node_id] = pre_node
                a, b, c = self.find_next_flow(flow_node_id, cur_intent, recorder, copy.deepcopy(trace), cnt_message)
                if a:
                    return a, b, c
            recorder = copy.deepcopy(recorder_old)
            trace = copy.deepcopy(trace_old)
        return None, None, recorder_old

    @staticmethod
    def get_condition_data(desc):
        tmp = desc.split(":", 1)
        if len(tmp) != 2:
            args = None
        else:
            desc, args = tmp[0], tmp[1]
        return desc, args

    def select_flow(self, tracker, pre_action, cur_intent, recorder, model_intent):
        old_recorder = copy.deepcopy(recorder)
        recorder.update_from_tracker(tracker)

        logger.info(f"[FLOW SELECTOR][{recorder.sender_id}] Current predicted intent: {cur_intent}")

        next_action, trace, recorder = self.find_next_flow(pre_action, cur_intent, recorder, {}, 0)
        if next_action:
            blocks, trace = self.get_blocks(pre_action, next_action, trace)
        else:
            next_action = "action_quit"
            blocks = ["action_quit"]

        # recorder update
        recorder = copy.deepcopy(old_recorder)
        condition_ids = []
        for node_id in trace:
            node = self.graph_controller.get_node(node_id)
            type_node = node.get("type_node")
            if type_node == "condition_flow":
                if node.get("params").get("check"):
                    for condition in node.get("params").get("check"):
                        condition_id = DrawioExtractor.extract_condition_code(condition)
                        condition_ids.append((condition_id, None))
                if node.get("params").get("update"):
                    for condition in node.get("params").get("update"):
                        condition_id = DrawioExtractor.extract_condition_code(condition)
                        condition_ids.append((condition_id, None))
            elif type_node == "update_local":
                for condition in node.get("params"):
                    condition_id = DrawioExtractor.extract_condition_code(condition)
                    condition_ids.append((condition_id, None))
            elif type_node == "fixed_condition":
                desc = node.get("desc")
                desc = remove_condition_id(desc)
                condition_id, args = self.get_condition_data(desc)
                if condition_id in ["checkpoint", "Checkpoint"]:
                    recorder.update_checkpoint()
                if condition_id in FIXED_CONDITIONS:
                    condition_ids.append((condition_id, args))
            recorder.update_trace_node(node_id)
        recorder.update(
            tracker=tracker,
            cur_action=next_action,
            cur_intent=cur_intent,
            cur_blocks=blocks,
            model_intent=model_intent,
            condition_ids=condition_ids
        )
        recorder.reset_update_local()
        return next_action, recorder

    def get_blocks(self, pre_action, next_action, trace):
        trace_new = []
        blocks = [next_action]
        trace_new.append(next_action)
        while trace[next_action] != pre_action:
            next_action = trace[next_action]
            type_next_action = self.graph_controller.get_type_of_node(next_action)
            if type_next_action in VERTICES:
                blocks.append(next_action)
            trace_new.append(next_action)
        blocks.reverse()
        trace_new.reverse()
        return blocks, trace_new
