import json
import logging

from src.core.condition.interfaces.impl_record import ImplUpdateGlobal, ImplCheckFlow
from config_platform import (
    PATH_FIXED_FLUENCY_ERROR,
    PATH_FIXED_ACCEPT_FORMAT,
    PATH_FIXED_ACCEPT_INTENT,
    PATH_FIXED_ACCEPT_SENTENCE
)
from src.dev.context.custom_condition_classes import regex_by_pattern
from src.dev.modules.module_check_error import *

check_error = ModuleCheckERROR()

logger = logging.getLogger(__name__)


class CorrectAnswer(ImplCheckFlow):
    def __init__(self):
        super().__init__()

    def name(self):
        return "correct_answer"

    def process(self, recorder, args):
        return True


class IncorrectAnswer(ImplCheckFlow):
    def __init__(self):
        super().__init__()

    def name(self):
        return "incorrect_answer"

    def process(self, recorder, args):
        return True


class ExtractFluencyError(ImplUpdateGlobal):
    def __init__(self):
        super().__init__()
        with open(PATH_FIXED_FLUENCY_ERROR, "r") as file_in:
            self.rules = json.load(file_in)

    def name(self):
        return "extract_fluency_error"

    def process(self, recorder, args):
        checkpoint = recorder.get_current_record().get("checkpoint")
        id_question = f"question_{checkpoint}"
        rule = self.rules.get(id_question)
        intents = rule.get("intents")
        error = None
        if recorder.cur_intent in intents:
            message = recorder.get_last_message()
            error = check_error.process_check_fluency(message, id_question, "")
        return error


class ExtractWordingError(ImplUpdateGlobal):
    def __init__(self):
        super().__init__()

    def name(self):
        return "extract_wording_error"

    def process(self, recorder, args):
        checkpoint = recorder.get_current_record().get("checkpoint")
        id_question = f"question_{checkpoint}"
        message = recorder.get_last_message()
        error = check_error.process_check_wording(message, id_question)
        return error


class AcceptIntent(ImplCheckFlow):
    def __init__(self):
        super().__init__()
        with open(PATH_FIXED_ACCEPT_INTENT, "r") as file_in:
            self.rules = json.load(file_in)

    def name(self):
        return "accept_intent"

    def process(self, recorder, args):
        answer = False
        for rule in self.rules:
            params = rule.get("params")
            intents = rule.get("intents")
            check_params = True

            for param in params:
                values = params.get(param)
                accept_value = recorder.get_current_record().get(param)

                if values and accept_value not in values:
                    check_params = False
                    break
            if check_params:
                if recorder.cur_intent in intents:
                    answer = True
                    break
        return answer


class AcceptFormat(ImplCheckFlow):
    def __init__(self):
        super().__init__()
        with open(PATH_FIXED_ACCEPT_FORMAT, "r") as file_in:
            self.rules = json.load(file_in)

    def name(self):
        return "accept_format"

    def process(self, recorder, args):
        answer = False
        for rule in self.rules:
            params = rule.get("params")
            intents = rule.get("intents")
            check_params = True
            for param in params:
                values = params.get(param)
                accept_value = recorder.get_current_record().get(param)
                if values and accept_value not in values:
                    check_params = False
                    break
            if check_params:
                if recorder.cur_intent in intents:
                    pattern = rule.get("pattern")
                    check_format = regex_by_pattern(pattern, recorder.get_last_message())
                    if check_format:
                        answer = True
                        break
        return answer


class AcceptSentence(ImplCheckFlow):
    def __init__(self):
        super().__init__()
        with open(PATH_FIXED_ACCEPT_SENTENCE, "r") as file_in:
            self.rules = json.load(file_in)

    def name(self):
        return "accept_sentence"

    def process(self, recorder, args):
        answer = False
        for rule in self.rules:
            params = rule.get("params")
            intents = rule.get("intents")
            check_params = True
            for param in params:
                values = params.get(param)
                accept_value = recorder.get_current_record().get(param)
                if values and accept_value not in values:
                    check_params = False
                    break
            if check_params:
                if recorder.cur_intent in intents:
                    pattern = rule.get("pattern")
                    check_format = regex_by_pattern(pattern, recorder.get_last_message())
                    if check_format:
                        answer = True
                        break
        return answer


class CheckBackendError(ImplCheckFlow):
    def __init__(self):
        super().__init__()

    def name(self):
        return "check_backend_error"

    def process(self, recorder, args):
        is_correct = True
        user_metadata = recorder.get_last_metadata().get("user_metadata")
        check_correct = user_metadata.get("check_correct")
        if check_correct:
            is_correct = check_correct.get("is_correct")
        return is_correct is False


class CheckBreakCheckpoint(ImplCheckFlow):
    def __init__(self):
        super().__init__()

    def name(self):
        return "check_break_checkpoint"

    def process(self, recorder, args):
        current_checkpoint = recorder.get_current_record().get("checkpoint")
        if recorder.get_user_info_record().get("number_of_checkpoints"):
            try:
                if current_checkpoint < int(recorder.get_user_info_record().get("number_of_checkpoints")):
                    return True
                else:
                    return False
            except Exception as e:
                pass
        return True


class CheckRecord(ImplCheckFlow):
    def __init__(self):
        super().__init__()

    def name(self):
        return "check_record"

    def process(self, recorder, args):
        try:
            args = json.loads(args)
        except Exception as e:
            logger.debug(f"Check Record failed: {e}")
            return False
        cur_record = recorder.get_current_record()
        check_flow = True
        for key in args:
            value = args.get(key)
            if key not in cur_record or value != cur_record.get(key):
                check_flow = False
                break
        return check_flow


class CheckPoint(ImplUpdateGlobal):
    def __init__(self):
        super().__init__()
        self.record = 0

    def name(self):
        return "checkpoint"

    def process(self, recorder, args):
        return self.record + 1


class RepeatCount(ImplUpdateGlobal):
    def __init__(self):
        super().__init__()
        self.record = {}

    def name(self):
        return "repeat_count"

    def process(self, recorder, args):
        """
        check_update and update from values of recorder
        :param recorder: Recorder
        :return: bool
        :desc: [89] Câu có lỗi sai ngữ pháp / phát âm
        """
        logger.debug(f"TRACE NODES: {recorder.trace_nodes}")
        pre_node = recorder.trace_nodes
        if pre_node:
            pre_node = pre_node[-1]
            if pre_node not in self.record:
                self.record[pre_node] = 0
            self.record[pre_node] += 1
        return self.record
