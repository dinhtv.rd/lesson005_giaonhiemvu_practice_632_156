import logging

from typing import Text, Dict, Any

from src.dev.context.const.recorder import Recorder
from src.core.database.redis_connection import RedisTable
from src.core.utils.io import jsonio
from config import (
    ENDPOINT_REDIS
)

logger = logging.getLogger(__name__)


class ContextStore:
    """
    manager list info need store to DB
    """

    def __init__(self, conversation_id, recorder=None):
        """
        Create list info need store to DB
        :param conversation_id:
        :param recorder: recorder object to store
        """
        self.conversation_id = conversation_id
        if recorder is None:
            recorder = Recorder(self.conversation_id)
        self.tickets = {
            "recorder": recorder
        }

    def dumps(self):
        tickets = {}
        for key in self.tickets:
            tickets[key] = self.tickets[key].dumps()
        return jsonio.dumps_utf8(dict(conversation_id=self.conversation_id, tickets=tickets))

    @classmethod
    def from_dict(cls, data: Dict[Text, Any]) -> "ContextStore":
        tickets = data.get("tickets")
        recorder = Recorder.from_dict(data.get("conversation_id"), tickets.get("recorder"))

        return cls(data.get("conversation_id"),
                   recorder
                   )

    def get_ticket(self, ticket_name):
        return self.tickets[ticket_name]


class RedisStore:
    """Redis store for ticket locks."""

    def __init__(self):
        self.redis_table = RedisTable(config=ENDPOINT_REDIS, table="recorder")

    def check_available_conversation_id(self, conversation_id: Text) -> bool:
        serialised_data = self.redis_table.get(conversation_id)
        answer = True if serialised_data else False
        return answer

    def get_data(self, conversation_id: Text) -> "ContextStore":
        serialised_data = self.redis_table.get(conversation_id)
        if not serialised_data:
            new_data = ContextStore(conversation_id)
            self.save_data(new_data)
            return new_data
        return ContextStore.from_dict(jsonio.loads_utf8(serialised_data))

    def save_data(self, data: ContextStore) -> None:
        self.redis_table.set(data.conversation_id, data.dumps())

    def delete(self, conversation_id: Text) -> None:
        self.redis_table.delete(conversation_id)
