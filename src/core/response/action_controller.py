import copy
import json

from src.core.utils.io import jsonio
from src.core.database.context_store import RedisStore, ContextStore
from src.core.policies.graph_loader import GraphLoader
from src.core.initializer.drawio_extractor import DrawioExtractor
from src.dev.context.define import CONFIG_RECORDS
from config import PROJECT_NAME_CONFIG

import logging

logger = logging.getLogger(__name__)


class ActionController(object):
    def __init__(self):
        self.conditions = copy.deepcopy(CONFIG_RECORDS)
        self.RESPONSE_FORM = {
            "status": "CHAT",
            "blocks": [],
            "variables": {},
            "action": "abc",
            "intent": "abc",
            "confidence": 1.0
        }
        self.BLOCK_FORM = {
            "text": {},
            "text_tts": {},
            "type": "bilingual_action"
        }
        self.redis_more_store = RedisStore()
        self.graph_controller = GraphLoader()

    def get_recorder(self, conversation_id):
        recorder = self.redis_more_store.get_data(conversation_id).tickets.get("recorder")
        return recorder

    async def execute_normal_action(self, tracker, cur_action):
        response = copy.deepcopy(self.RESPONSE_FORM)
        restart = False

        recorder = self.get_recorder(tracker.sender_id)
        blocks = recorder.cur_blocks
        if blocks:
            for node_id in blocks:
                action_node = self.graph_controller.get_node(node_id)
                block = copy.deepcopy(self.BLOCK_FORM)
                if node_id != "action_quit":
                    type_node = action_node.get("type_node")
                    block["type_node"] = type_node
                    params_node = action_node.get("params")
                    logger.info(f"[BLOCK RESPONSE] Node: {params_node}")
                    if type_node == "end_node":
                        restart = True
                        response["status"] = "END"
                        break
                    elif type_node == "correction_node":
                        user_metadata = recorder.get_last_metadata().get("user_metadata")
                        check_correct = user_metadata.get("check_correct")
                        if check_correct:
                            is_correct = check_correct.get("is_correct")
                            correction_guide = check_correct.get("correction_guide")
                            if not is_correct and correction_guide:
                                response["blocks"].extend(correction_guide)
                    elif type_node == "api_node":
                        pass
                    else:
                        code = node_id
                        block["text"]["en"], _ = self.fill_slot_to_text(params_node.get("text_en")[0],
                                                                        recorder,
                                                                        None,
                                                                        "en")
                        block["text"]["vi"], _ = self.fill_slot_to_text(params_node.get("text_vi")[0],
                                                                        recorder,
                                                                        None,
                                                                        "vi")
                        block["text_tts"]["en"], code_en = self.fill_slot_to_text(params_node.get("tts_en")[0],
                                                                                  recorder,
                                                                                  code,
                                                                                  "en")
                        block["text_tts"]["vi"], code_vi = self.fill_slot_to_text(params_node.get("tts_vi")[0],
                                                                                  recorder,
                                                                                  code,
                                                                                  "vi")
                        for key in params_node:
                            if key not in ["text_en", "text_vi", "tts_en", "tts_vi"]:
                                block[key] = params_node.get(key)[0]
                        image = params_node.get("image")
                        if image:
                            image = image[0]
                        if image:
                            if image == "CLEAR":
                                image = None
                        else:
                            image = recorder.last_image
                        block["image"] = image
                        recorder.last_image = image

                        block["audio_id_en"] = code_en
                        block["audio_id_vi"] = code_vi
                        if type_node == "bilingual_action":
                            block["tts_audio_id"] = code_vi
                        else:
                            block["tts_audio_id"] = code_en
                        block["tts_voice_id"] = block["voice_id"]

                        if isinstance(block, dict) and isinstance(block.get("audio_play"), str):
                            if block["audio_play"] == "False":
                                block["audio_play"] = False
                            if block["audio_play"] == "True":
                                block["audio_play"] = True

                        if "display" in block:
                            block["type"] = block["display"]
                            del (block["display"])
                        del (block["voice_id"])
                        response["blocks"].append(copy.deepcopy(block))
                else:
                    raise Exception("Action quit executed! something wrong! please check the flow!")

        logger.info(f"[BLOCL RESPONSE] {tracker.sender_id} - Record after executed action:"
                     f" {json.dumps(recorder.get_current_record(), indent=4)}")

        variables = {}
        v1 = recorder.get_current_record().copy()
        v2 = recorder.get_user_info_record().copy()
        for key in v1:
            variables[key] = v1.get(key)
        for key in v2:
            variables[key] = v2.get(key)

        progress = 0
        checkpoint = recorder.get_current_record().get("checkpoint")
        total_checkpoint = self.graph_controller.total_checkpoints + 1
        if recorder.get_user_info_record() and recorder.get_user_info_record().get("number_of_checkpoints"):
            total_checkpoint = recorder.get_user_info_record().get("number_of_checkpoints") + 1
        if total_checkpoint > 0:
            if restart:
                checkpoint += 1
            progress = round(checkpoint / total_checkpoint * 100)

        response["progress"] = progress
        response["variables"] = variables

        response["action"] = self.graph_controller.get_node(recorder.cur_action).get("desc")
        response["intent"] = recorder.cur_intent
        response["confidence"] = tracker.current_state().get("latest_message").get("intent").get("confidence")
        self.redis_more_store.save_data(ContextStore(tracker.sender_id, recorder=recorder))
        logger.info(f"[BLOCK RESPONSE] {response}")
        return jsonio.dumps_utf8(response), restart

    def fill_slot_to_text(self, text, recorder, code, language):
        if code:
            code = f"{language}_{PROJECT_NAME_CONFIG.get('bot_name')}_" \
                   f"{PROJECT_NAME_CONFIG.get('task_name')}_" \
                   f"{PROJECT_NAME_CONFIG.get('project_version')}_" \
                   f"{code}"

        slots = DrawioExtractor.get_slots_from_text(text)

        for idx, slot in enumerate(slots):
            condition_id = DrawioExtractor.extract_condition_code(slot)
            condition_value = recorder.get_current_record().get(condition_id)
            if code:
                index_slot = self.conditions.get(condition_id).slots_for_init_text().index(condition_value)
                code += f"_{condition_id}_{index_slot}"
            if condition_value.startswith("user_info:"):
                condition_value = recorder.get_user_info_record().get(condition_value.replace("user_info:", ""))
            text = text.replace("{" + f"{slot}" + "}", condition_value)

        return text, code
