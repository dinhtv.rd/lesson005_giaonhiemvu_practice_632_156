import os
import yaml
import json
import pickle
import logging

PATH_IDENTIFIER = "identifier.yml"

with open(PATH_IDENTIFIER, "r") as identifier_file:
    PROJECT_NAME_CONFIG = yaml.safe_load(identifier_file)

logger = logging.getLogger(__name__)

VERSION = "2.0"
SESSION_EXPIRATION_TIME = 60
CARRY_OVER_SLOTS_TO_NEW_SESSION = "true"
DB_TEST_MODE = False
DB_TEST_SENDER_ID = "test-conversation"
MAX_INTENTS = 100000
DEFAULT_NAME_INTENT_FALLBACK = "intent_fallback"
DEFAULT_ENCODING = "utf8"
DEFAULT_THRESHOLD = 0.45
DEFAULT_THRESHOLD_SHORT_MESSAGE = 0.6
DEFAULT_FALLBACK_INTENTS = [
    "intent_fallback"
]

# Domain config
DOMAIN_VERSION = "version"
DOMAIN_SESSION_CONFIG = "session_config"
DOMAIN_INTENTS = "intents"
DOMAIN_ACTIONS = "actions"
DOMAIN_ENTITIES = "entities"
DOMAIN_SLOTS = "slots"
DOMAIN_START_ACTIONS = "start_actions"
DOMAIN_START_INTENTS = "start_intents"
DOMAIN_FINISH_ACTIONS = "finish_actions"
DOMAIN_LIMIT_SLOTS = "limit_slots"
DOMAIN_QUERY_ACTIONS = "query_actions"

DOMAIN_AUTO_CONFIG = {
    DOMAIN_INTENTS: [],
    DOMAIN_ACTIONS: [],
    DOMAIN_ENTITIES: [],
    DOMAIN_SLOTS: {},
    DOMAIN_START_ACTIONS: [],
    DOMAIN_START_INTENTS: [],
    DOMAIN_FINISH_ACTIONS: [],
    DOMAIN_LIMIT_SLOTS: {},
}

DOMAIN_MANUAL_CONFIG = {
    DOMAIN_VERSION: "2.0",
    DOMAIN_SESSION_CONFIG: {
        "session_expiration_time": 60,
        "carry_over_slots_to_new_session": True,
    },
    DOMAIN_QUERY_ACTIONS: [],
}

DIR_CONFIG = "config"
DIR_RESOURCES = "resources"

# config resources: nlu, ner, template
DIR_CONFIG_RESOURCES = os.path.join(DIR_RESOURCES, "config")
DIR_NLU_RESOURCE = os.path.join(DIR_CONFIG_RESOURCES, "nlu")
DIR_NER_RESOURCE = os.path.join(DIR_NLU_RESOURCE, "ner")
DIR_TEMPLATE = os.path.join(DIR_CONFIG_RESOURCES, "templates")

# output: graph.html
DIR_OUTPUT = os.path.join(DIR_RESOURCES, "outputs")

# config for dev
PATH_CONFIG_REGEX = os.path.join(DIR_CONFIG, "regex.json")
PATH_CONFIG_USER_INFO = os.path.join(DIR_CONFIG, "user_info.json")

# const
PATH_CONFIG_FALLBACK_WORDS = os.path.join(DIR_CONFIG_RESOURCES, "fallback_words.txt")
PATH_CONFIG_STOP_WORDS = os.path.join(DIR_CONFIG_RESOURCES, "stop_words.json")
PATH_REPLACEMENT_DICTIONARY = os.path.join(DIR_CONFIG_RESOURCES, "dictionary.json")

PATH_ACTIONS_PY = "actions.py"
PATH_DOMAIN = "domain.yml"
PATH_ENDPOINTS = "endpoints.yml"

with open(PATH_ENDPOINTS, "r") as endpoints_file:
    endpoints = yaml.safe_load(endpoints_file)

    ENDPOINT_SERVER = endpoints.get("server_endpoint").get("url")
    ENDPOINT_ACTION = endpoints.get("action_endpoint").get("url")
    ENDPOINT_NER_PERSONAL_NAME = endpoints.get("ner_personal_name").get("url")
    ENDPOINT_NER_ADDRESS = endpoints.get("ner_address").get("url")
    ENDPOINT_VOICE_BIO = endpoints.get("voice_bio")
    ENDPOINT_DUCKLING = endpoints.get("duckling")
    ENDPOINT_DATABASE = endpoints.get("database")
    ENDPOINT_DATABASE["db"] = f"{PROJECT_NAME_CONFIG.get('bot_name')}_{PROJECT_NAME_CONFIG.get('task_name')}"
    ENDPOINT_REDIS = endpoints.get("redis")
    ENDPOINT_REDIS["name_db"] = f"{PROJECT_NAME_CONFIG.get('bot_name')}_{PROJECT_NAME_CONFIG.get('task_name')}"

with open(PATH_DOMAIN, "r") as config_domain:
    CONFIG_DOMAIN = yaml.safe_load(config_domain)

with open(PATH_CONFIG_USER_INFO, "r", encoding="utf-8") as config_user_info:
    DEFAULT_USER_INFO = json.load(config_user_info)

DB_KEY_HISTORY = "table_history"
DB_KEY_USER_INFORMATION = "table_user_information"
DB_KEY_FLOW_TESTCASE = "table_flow_testcase"

if __name__ == "__main__":
    ks = [k for k in dir() if (k[:2] != "__" and not callable(globals()[k]))]
    for k in ks:
        print(f'  {k}:  {str(globals()[k])}')
