import os

# config for initializer drawio
DIR_CONFIG = "config"
DIR_INITIALIZER = os.path.join(DIR_CONFIG, "graph_rule")
DIR_DUMP = os.path.join(DIR_CONFIG, "drawio_dumped")
PATH_VALIDATE_FORMAT = os.path.join(DIR_INITIALIZER, "validate.json")
PATH_DETERMINER = os.path.join(DIR_INITIALIZER, "determiner.json")
PATH_CONNECTION_RULE = os.path.join(DIR_INITIALIZER, "connection_rule.json")

PATH_NODES_DUMPED = os.path.join(DIR_DUMP, "nodes.json")
PATH_EDGES_DUMPED = os.path.join(DIR_DUMP, "edges.json")
PATH_EDGES_FULL_DUMPED = os.path.join(DIR_DUMP, "edges_full.json")
PATH_CONDITIONS_DUMPED = os.path.join(DIR_DUMP, "conditions.json")
PATH_API_METADATA_DUMPED = os.path.join(DIR_DUMP, "api_metadata.json")
PATH_PRIORITY_RULE = os.path.join(DIR_INITIALIZER, "priority.json")

# config for template
DIR_RESOURCES = "resources"
DIR_CONFIG_RESOURCES = os.path.join(DIR_RESOURCES, "config")
DIR_TEMPLATE = os.path.join(DIR_CONFIG_RESOURCES, "templates")
DIR_TEMPLATE_CONDITION = os.path.join(DIR_TEMPLATE, "conditions")
DIR_GRAPHS = os.path.join(DIR_RESOURCES, "graphs")

PATH_CHECK_CLASS_TEMPLATE = os.path.join(DIR_TEMPLATE_CONDITION, "check_class.template")
PATH_UPDATE_LOCAL_CLASS_TEMPLATE = os.path.join(DIR_TEMPLATE_CONDITION, "update_local_class.template")
PATH_UPDATE_GLOBAL_CLASS_TEMPLATE = os.path.join(DIR_TEMPLATE_CONDITION, "update_global_class.template")
PATH_UPDATE_SLOT_CLASS_TEMPLATE = os.path.join(DIR_TEMPLATE_CONDITION, "update_slot_class.template")
PATH_HEADER_CONDITIONS_TEMPLATE = os.path.join(DIR_TEMPLATE_CONDITION, "header_conditions.template")
PATH_API_METADATA_TEMPLATE = os.path.join(DIR_TEMPLATE_CONDITION, "api_metadata_class.template")
PATH_HEADER_API_METADATA_TEMPLATE = os.path.join(DIR_TEMPLATE_CONDITION, "header_api_metadata.template")

# config for log
DIR_LOGS = os.path.join(DIR_RESOURCES, "logs")
PATH_INIT_LOGS = os.path.join(DIR_LOGS, "init.log")

# config for core python file
DIR_CORE = "src/core"
DIR_DEV = "src/dev"

PATH_CONDITION_CLASSES = os.path.join(DIR_DEV, "context/condition_classes.py")
PATH_API_METADATA_CLASSES = os.path.join(DIR_CORE, "api_metadata/api_metadata_classes.py")
PATH_GRAPH_DUMPED = os.path.join(DIR_GRAPHS, "graph_dumped.xml")

# config for fixed condition
DIR_FIXED_CONDITION_CONFIG = os.path.join(DIR_CONFIG, "fixed_condition_config")
PATH_FIXED_FLUENCY_ERROR = os.path.join(DIR_FIXED_CONDITION_CONFIG, "fluency_error.json")
PATH_FIXED_ACCEPT_FORMAT = os.path.join(DIR_FIXED_CONDITION_CONFIG, "accept_format.json")
PATH_FIXED_ACCEPT_INTENT = os.path.join(DIR_FIXED_CONDITION_CONFIG, "accept_intent.json")
PATH_FIXED_ACCEPT_SENTENCE = os.path.join(DIR_FIXED_CONDITION_CONFIG, "accept_sentence.json")

# Domain config
DOMAIN_VERSION = "version"
DOMAIN_SESSION_CONFIG = "session_config"
DOMAIN_INTENTS = "intents"
DOMAIN_ACTIONS = "actions"
DOMAIN_ENTITIES = "entities"
DOMAIN_SLOTS = "slots"
DOMAIN_START_ACTIONS = "start_actions"
DOMAIN_START_INTENTS = "start_intents"
DOMAIN_FINISH_ACTIONS = "finish_actions"
DOMAIN_LIMIT_SLOTS = "limit_slots"
DOMAIN_QUERY_ACTIONS = "query_actions"
DOMAIN_AUTO_CONFIG = {
    DOMAIN_INTENTS: [],
    DOMAIN_ACTIONS: [],
    DOMAIN_ENTITIES: [],
    DOMAIN_SLOTS: {},
    DOMAIN_START_ACTIONS: [],
    DOMAIN_START_INTENTS: [],
    DOMAIN_FINISH_ACTIONS: [],
    DOMAIN_LIMIT_SLOTS: {},
}
DOMAIN_MANUAL_CONFIG = {
    DOMAIN_VERSION: "2.0",
    DOMAIN_SESSION_CONFIG: {
        "session_expiration_time": 60,
        "carry_over_slots_to_new_session": True,
    },
    DOMAIN_QUERY_ACTIONS: [],
}

PATH_ACTIONS_PY = "actions.py"
PATH_DOMAIN = "domain.yml"

# config node
VERTICES = [
    "start_node",
    "end_node",
    "bilingual_action",
    "english_action",
    "api_node",
    "correction_node"
]
FLOWS = [
    "condition_flow",
    "intent_flow",
    "update_local",
    "fixed_condition"
]

FIXED_CONDITIONS = [
    "correct_answer",
    "incorrect_answer",
    "extract_fluency_error",
    "extract_wording_error",
    "accept_intent",
    "accept_format",
    "accept_sentence",
    "check_backend_error",
    "check_record",
    "check_break_checkpoint"
]

FIXED_CHECK_CONDITIONS = [
    "accept_intent",
    "accept_format",
    "accept_sentence",
    "check_backend_error",
    "check_record",
    "check_break_checkpoint"
]

FIXED_UPDATE_CONDITIONS = [
    "correct_answer",
    "incorrect_answer",
    "extract_fluency_error",
    "extract_wording_error"
]
